﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportOOP
{
    abstract class AbstractClass
    {
        public abstract void UpdateData(string Model);
        public abstract void UpdateData(double Price);
    }
}
