﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            People person = new People("Alex", 20);
            person.Action();
            person.Move();
            Console.WriteLine();

            Passager passager= new Passager("Tudor",25,"IAWDA20");
            passager.Action();
            passager.Move();
            passager.Work();
            Console.WriteLine();

            TicketColletor ticketColletor = new TicketColletor("Mircea", 30, 100);
            ticketColletor.SellTicket();
            ticketColletor.ShowData();
            Console.WriteLine();

            Vehicle vehicle = new Vehicle("Audi", 40000);
            vehicle.ShowData();
            vehicle.UpdateData("Mazda");
            vehicle.ShowData();
            Console.WriteLine();

            Buss buss = new Buss("dawd", 2000);
            buss.ShowData();
            buss.UpdateData(200);
            buss.UpdateData("Mercedes");
            buss.ShowData();

            Console.ReadKey();

        }
    }
}
