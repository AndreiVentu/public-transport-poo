﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportOOP
{
    class Buss:Vehicle
    {
        public Buss()
        {
            Console.WriteLine("Buss default constructor");
        }

        public Buss(string Model, double Price)
        {
            this.Model = Model;
            this.Price = Price;
            Console.WriteLine("Buss with parameter constructor");
        }

        public override void UpdateData(string Model)
        {
            this.Model = Model;
        }

        public override void UpdateData(double Price)
        {
            if (Price > 0)
            {
                this.Price = Price;
            }

        }

        public void ShowData()
        {
            Console.WriteLine(Model + " " + Price);
        }
    }
}
