﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportOOP
{
    class People:Interface
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public People()
        {
            Console.WriteLine("People default constructor");
        }

        public People(string Name, int Age)
        {
            this.Name = Name;
            this.Age = Age;
            Console.WriteLine("People with parameter constructor");
        }

        public void Action()
        {
            Console.WriteLine("People action");
        }

        public void Work()
        {
            Console.WriteLine("People work");
        }

        public void Move()
        {
            Console.WriteLine("People move");
        }
    }
}
