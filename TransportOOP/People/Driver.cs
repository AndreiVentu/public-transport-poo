﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportOOP
{
    class Driver:People
    {
        public string DrivingLicense { get; set; }
        public Driver()
        {
            Console.WriteLine("Driver default constructor");
        }

        public Driver(string Name, int Age, string DrivingLicense)
        {
            this.Name = Name;
            this.Age = Age;
            this.DrivingLicense = DrivingLicense;
            Console.WriteLine("Driver parameter constructor");
        }

        public new void Action()
        {
            Console.WriteLine("Driver action");
        }

        public new void Move()
        {
            Console.WriteLine("Driver move");
        }

        public new void Work()
        {
            Console.WriteLine("Driver work");
        }

    }
}
