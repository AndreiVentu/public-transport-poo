﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportOOP
{
    class TicketColletor:People
    {
        public int TicketsNumber { get; set; }
        public TicketColletor()
        {
            Console.WriteLine("TicketColletor default constructor");
        }

        public TicketColletor(string Name, int Age, int TicketsNumber)
        {
            this.Name = Name;
            this.Age = Age;
            this.TicketsNumber = TicketsNumber;
            Console.WriteLine("TicketColletor parameter constructor");
        }

        public new void Action()
        {
            Console.WriteLine("TicketColletor action");
        }

        public new void Move()
        {
            Console.WriteLine("TicketColletor move");
        }

        public new void Work()
        {
            Console.WriteLine("TicketColletor work");
        }

        public void SellTicket()
        {
            if(TicketsNumber>0)
            TicketsNumber--;
        }

        public void ShowData()
        {
            Console.WriteLine(Name + " " + Age + " Number of tickets: " + TicketsNumber);
        }
    }
}
