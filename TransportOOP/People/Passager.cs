﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportOOP
{
    class Passager:People
    {
        public string Ticket { get; set; }
        public Passager()
        {
            Console.WriteLine("Passager default constructor");
        }

        public Passager(string Name, int Age, string Ticket)
        {
            this.Name = Name;
            this.Age = Age;
            this.Ticket = Ticket;
            Console.WriteLine("Passager parameter constructor");
        }

        public new void Action()
        {
            Console.WriteLine("Passager action");
        }

        public new void Move()
        {
            Console.WriteLine("Passager move");
        }

        public new void Work()
        {
            Console.WriteLine("Passager work");
        }
    }
}
